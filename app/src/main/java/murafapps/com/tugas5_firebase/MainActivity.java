package murafapps.com.tugas5_firebase;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button Bregis;
    private EditText etEmail;
    private EditText etPass;
    private TextView tvSign;

    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);

        Bregis = (Button) findViewById(R.id.Bregis);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPass = (EditText) findViewById(R.id.etPass);

        tvSign = (TextView) findViewById(R.id.tvSign);

        Bregis.setOnClickListener(this);
        tvSign.setOnClickListener(this);
    }

    private void registerUser(){
        String sEmail = etEmail.getText().toString().trim();
        String sPass = etPass.getText().toString().trim();

        if(TextUtils.isEmpty(sEmail)){
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(sPass)){
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.setMessage("Registering User ...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(sEmail, sPass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            //user is successfully registered and logged in

                            Toast.makeText(MainActivity.this, "Daftar berhasil", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(MainActivity.this, "Daftar gagal", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }

    
    @Override
    public void onClick(View v) {
        if(v == Bregis){
            registerUser();
        }
        if(v == tvSign){
        }
    }
}
